<?php

namespace App\Controller;

use App\Entity\Car;
use App\Form\CarType;
use App\Entity\Keyword;
use App\Repository\CarRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(CarRepository $repoCar)
    {
        $cars = $repoCar->findAll();
       
        return $this->render('index/index.html.twig', [
            'cars' => $cars,
        ]);
    }

    /**
     * @Route("/add", name="add")
     */
    public function add(EntityManagerInterface $manager, Request $request)
    {
        // $car = new Car();
        // $car->setPrice('2500');
        // $car->setModel('Laguna');

        // $keywordSpacieuse = new Keyword();
        // $keywordSpacieuse->setName('spacieuse');
        
        // $keywordNonFumeur = new Keyword();
        // $keywordNonFumeur->setName('fumeur');

        // $car->addKeywords($keywordSpacieuse);
        // $car->addKeywords($keywordNonFumeur);

        // $manager->persist($car);
        // $manager->flush();

        //    $this->addFlash(
        //        'notice', 
        //        'Votre voiture a bien été ajoutée!!'
        //    );

        //    return $this->redirectToRoute('index');




       $form = $this->createForm(CarType::class);

       $form->handleRequest($request); 
       if($form->isSubmitted() && $form->isValid()){
           $path = $this->getParameter('kernel.project_dir') .'/public/image';
           $car = $form->getData();

           $image = $car->getImage();
           
           $file = $image->getFile();

           $name = md5(uniqid()). '.'.$file->guessExtension();

           $file->move($path, $name);

           $image->setName($name);





           $manager->persist($car);
           $manager->flush();

           $this->addFlash(
               'notice', 
               'Votre voiture a bien été ajoutée!!'
           );

           return $this->redirectToRoute('index');
       }
        
        return $this->render('index/add.html.twig', [
            'form'=> $form->createView()
        ]);
    }

    /**
     * @Route("/show/{id}", name="show")
     */
    public function show(Car $car)
    {
       
        return $this->render('index/show.html.twig', [
            'car' => $car,
        ]);
    }

     /**
     * @Route("/edit/{id}", name="edit")
     */
    public function edit(Car $car, EntityManagerInterface $manager, Request $request)
    {
       
        $form = $this->createForm(CarType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
          $car = $form->getData();
          $manager->flush();
          $this->addFlash(
              'notice',
              'Super! voiture modifiée'
          );  
          return $this->redirectToRoute('index');
        }
       
        return $this->render('index/edit.html.twig', [
            'car' => $car,
            'form'=> $form->createView()
        ]);
    }

     /**
     * @Route("/delete/{id}", name="delete")
     */
    public function delete(Car $car, EntityManagerInterface $manager)
    {
       
        $manager->remove();
        $manager->flush();
       
        return $this->redirectToRoute('index');
    }

}
