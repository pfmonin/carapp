<?php

namespace App\Controller;

use App\Form\SearchCarType;
use App\Repository\CarRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SearchController extends AbstractController
{
    /**
     * @Route("/car/search", name="search_car")
     */
    public function searchCar(Request $request, CarRepository $carRepo)
    {
        $cars=[];
        $searchCarForm = $this->createForm(SearchCarType::class);
        if($searchCarForm->handleRequest($request)->isSubmitted() && $searchCarForm->isValid()){
            $criteria = $searchCarForm->getData();
            
            $cars = $carRepo->searchCar($criteria);
        }


        return $this->render('search/index.html.twig', [
            'search_form' => $searchCarForm->createView(),
            'cars'=>$cars,
            
        ]);
    }
}
